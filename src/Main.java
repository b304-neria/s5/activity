import com.neria.activity.*;

public class Main {
    public static void main(String[] args) {
        //9. Instantiate phonebook
        Phonebook phonebook = new Phonebook();

        //10. Instantiate 2 contacts (based from sample output)
        Contact johnDoe = new Contact();
        johnDoe.setName("John Doe");
        johnDoe.setContactNumber(new String[]{"+639152468596", "+639228547963"});
        johnDoe.setAddress(new String[]{"my home in Quezon City", "my office in Makati City"});

        Contact janeDoe = new Contact();
        janeDoe.setName("Jane Doe");
        janeDoe.setContactNumber(new String[]{"+639162148573", "+639173698541"});
        janeDoe.setAddress(new String[]{"my home in Caloocan City", "my office in Pasay City"});


        //11. Add contacts to phonebook
        phonebook.setContacts(johnDoe);
        phonebook.setContacts(janeDoe);


        //12. Control Structure
        if (phonebook.getContacts().size() == 0) {
            System.out.println("Phonebook is empty.Add more contacts!");
        } else {
            // Loop for each contact
            for (Contact contact : phonebook.getContacts()) {
                System.out.println(contact.getName());
                System.out.println("-----------------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                // Loop for each number
                for (String number : contact.getContactNumber()) {
                    System.out.println(number);
                }
                System.out.println("--------------------------------------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                // Loop for each address
                for (String address : contact.getAddress()) {
                    System.out.println(address);
                }
                System.out.println("========================================================");
                 }
             }
        }
}