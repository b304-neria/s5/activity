package com.neria.activity;

public class Contact {

    //3. Declare vars
    private String name;
    private String[] contactNumber;
    private String[] address;

    // 4. Default and Parameterized Constructors
    public Contact() {
    }
    public Contact(String name, String[] contactNumber, String[] address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    //5. Getters and Setters for vars
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String[] getContactNumber() {
        return contactNumber;
    }
    public void setContactNumber(String[] contactNumber) {
        this.contactNumber = contactNumber;
    }
    public String[] getAddress() {
        return address;
    }
    public void setAddress(String[] address) {
        this.address = address;
    }

}
