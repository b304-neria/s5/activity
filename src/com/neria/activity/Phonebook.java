package com.neria.activity;

import java.util.ArrayList;

public class Phonebook {
    //6. single instance variable
    private ArrayList<Contact> contacts;

    //7. Default and Parameterized Constructor
    public Phonebook() {
        contacts = new ArrayList<>();
    }
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
    //8. Getter and Setter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }
    public void setContacts(Contact contacts) {
        this.contacts.add(contacts);
    }
}
